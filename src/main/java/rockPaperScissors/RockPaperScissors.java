package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    String playerInput = "";
    String cpuInput = "";
    Boolean continueGame = true;
    Random random = new Random();
    
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
    	do {
	    	displayRoundText();
	    	cpuInput = getCpuChoice();
	    	playerInput = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
	    	while (!validChoiceInput(playerInput)) {
	    		playerInput = readInput("I do not understand " + playerInput + ". Could you try again?");
	    	}
	    	
	    	printChoices(playerInput, cpuInput);
	    	findWinnerAndUpdateScore(playerInput, cpuInput);
	    	printScore();
	    	continueGame = getContinueGame();
	    	roundCounter++;
    	
    	}while (continueGame);
    	
    	System.out.println("Bye bye :)");
    		
    		
    }
    
    public void displayRoundText() {
    	System.out.println("Let's play round " + roundCounter);
    }
    
    public Boolean validChoiceInput(String input) {
    	if (rpsChoices.contains(input)) {
    		return true;
    	}
    	else {
    		return false;
    	}
    }
    //does the first choice win against the second?
    public Boolean wins(String choiceOne, String choiceTwo) {
    	if (choiceOne.equals("rock")) {
    		return (choiceTwo.equals("scissors"));
    	}
    	else if (choiceOne.equals("scissors")) {
    		return (choiceTwo.equals("paper"));
    	}
    	
    	//choice one == paper
    	else {
    		return (choiceTwo.equals("rock"));
    	}
    	
    	
    }
    
    public void findWinnerAndUpdateScore(String playerChoice, String cpuChoice) {
    	if (wins(playerChoice, cpuChoice)) {
    		System.out.print("Human wins!");
    		humanScore++;
    		System.out.println();
    	}
    	else if (wins(cpuChoice, playerChoice)) {
    		System.out.print("Computer wins!");
    		computerScore++;
    		System.out.println();
    	}
    	else {
    		System.out.print("It's a tie!");
    		System.out.println();
    	}
    }
    
    public void printChoices(String playerChoice, String cpuChoice) {
    	System.out.print("Human chose " + playerChoice + ", " + "computer chose " + cpuChoice + ".");
    }
    
    public void printScore() {
    	System.out.println("Score: human " + humanScore + ", computer " + computerScore);
    }
    
    public Boolean getContinueGame() {
    	String input = "";
    	do {
    	input = readInput("Do you wish to continue playing? (y/n)?");
    	if (!(input.equals("y") || input.equals("n"))) System.out.println("Please answer y for yes, or n for no."); }
    	while (!(input.equals("y") || input.equals("n")));
    	return (input.equals("y"));
    	

    }
    
    public String getCpuChoice() {
    	int randomBound = (rpsChoices.size()) - 1;
    	int cpuChoice = random.nextInt(randomBound);
    	return rpsChoices.get(cpuChoice);
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }
    

}
